## Introduction ##
All tasks descriptions will be in this README.md file of the corresponding branch. The workflow should be:

Create a fork of this repository at bitbucket + give the bitbucket user Christian_Rommel access to your repo.

1. You decide which task you like to start.
2. Start working only on that task. Don't log more that the max hour specified. If you are experienced in some area, some task might go a lot faster. 
3. Push you final solution / answer to the corresponding feature branch. Written answers can be inserted inside the README.md directly behind the questions.
4. Drop us a message on skype + wait for our feedback.
5. Once you have our feedback -> start at 1. with the next task.

----------
## task 1 - git ##
* checkout branch task/1_git
* max 2h

----------
## task 2 - css ##
* checkout branch task/2_css
* value 4h


----------
## task 3 - node ##
* checkout branch task/3_node
* value 10h


----------
## task 4 - angular ##
* checkout branch task/4_angular
* value 8h